const inventory = require("../inventory"); //Importing inventory variable
const problem4 = require("../problem4"); //Importing problem4 function to AllCarYears
const old_Cars = require("../problem5"); //Importing problem5 function to old_Cars


// Calling functions to get and log older cars and their count
const carYears = problem4(inventory);
const olderCars = old_Cars(carYears);
console.log(olderCars);