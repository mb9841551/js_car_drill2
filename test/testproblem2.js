const inventory = require("../inventory"); //Importing inventory variable
const LastCar = require("../problem2"); //Importing problem2 function 

// Calling the function to get and log information about the last car in the inventory
const result = LastCar(inventory);

console.log(`Last car is a ${result.car_make} ${result.car_model}`);