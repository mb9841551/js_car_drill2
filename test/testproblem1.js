const inventory = require("../inventory"); //Importing inventory variable
const problem1 = require("../problem1"); //Importing problem1 function 

// Calling the function to find and log information about a car with id 33
const result = problem1(inventory , 33);

// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);