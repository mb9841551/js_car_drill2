// The marketing team wants the car models listed alphabetically on the website.
// Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function problem3(inventory) {
    // Using the map method to extract car models and sort method to sort them
    const sortedModels = inventory.map(function (index) {
      return index.car_model;
    }).sort();
    return sortedModels;
  }
  
  module.exports = problem3;