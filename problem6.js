// A buyer is interested in seeing only BMW and Audi cars within the inventory.
//  Execute a function and return an array that only contains BMW and Audi cars.
//  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function problem6(inventory) {
    // Using the filter method to find BMW and Audi cars
    const bmwAndAudiCars = inventory.filter(function (index) {
      return index.car_make === 'BMW' || index.car_make === 'Audi';
    });
    return bmwAndAudiCars;
  }
  
  module.exports = problem6;